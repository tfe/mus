"use strict";

class Game
{
    async init(root_container){
        try
        {
            let ux = new Ux({
                root: root_container
            });
            this.ux = ux;


            // Connect to the remote server
            let client = new Client();
            if(/https:/.test(location.href))
            {
                let s = await client.connect({
                    host:location.hostname+":6060",
                    ssl: true,
                    ux: ux
                });
            }
            else
            {
                let s = await client.connect({
                    host:"localhost:6060",
                    ssl: false,
                    ux: ux
                });
            }

            // Build DOM for the Game
            ux.build();
        }
        catch(err)
        {
            console.error('Main game error',err);
        }
    }
}

