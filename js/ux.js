"use strict";

class Ux
{

    constructor(params)
    {
        if(!params || !params.root)
        {
            console.log('error ',this, params);
            throw new Error('No root parameter received.');
        }

        let re;
        // DEBUG
        if(re = location.href.match(/level=(\d+)/))
        {
            this.current_level=re[1];
        }
    }

    log(item)
    {
        console.log('LOG: ',item);
    }

    build()
    {
        this.init();
    }

    result_setName()
    {
        console.log('result setname');
        document.querySelector('.config').classList.add('hidden');
        document.querySelector('.games').classList.remove('hidden');
    }
    setName()
    {
        let pseudo  = document.querySelector('#pseudo').value || 'Random'+(Math.floor(Math.random()*1000));
        this.send({ command:'setName', pseudo: pseudo});
    }
createGame()
    {
        let name  = document.querySelector('#game_name').value || 'Random game name'+(Math.floor(Math.random()*1000));
        let max  = document.querySelector('#max_players').value || 4;
        this.send({ command:'create', name: name, max: max });
    }

    joinGame()
    {
        let id  = document.querySelector('#game_list').value;
        if(id)
        {
            this.send({ command:'join', id: id});
        }
    }

    spectatorGame()
    {
        let id  = document.querySelector('#game_list').value;
        if(id)
        {
            this.send({ command:'spectator', id: id});
        }
    }

    nextGame()
    {
        this.send({ command:'nextGame'});
    }

    leaveGame()
    {
        console.log('should leave the game',this);
        this.send({ command:'leave'});
        document.querySelector('.games').classList.remove('hidden');
        document.querySelector('.game').classList.add('hidden');
        console.log('after leave');
    }

    socket_error(item)
    {
        alert(item.message);
    }

    send(item)
    {
        this.client.send(item);
    }


    init()
    {
        document.querySelector('#pseudo_set').addEventListener('click', (e) => this.setName());
        document.querySelector('#create').addEventListener('click', (e) => this.createGame());
        document.querySelector('#join').addEventListener('click', (e) => this.joinGame());
        document.querySelector('#spectator').addEventListener('click', (e) => this.spectatorGame());
        document.querySelector('#leave').addEventListener('click', (e) => this.leaveGame());
        document.querySelector('#pick_discarded').addEventListener('click', (e) => this.pickDiscarded());
        document.querySelector('#show_cards').addEventListener('click', (e) => this.showCards());
        document.querySelector('#next_round').addEventListener('click', (e) => this.nextGame());
        document.querySelector('.whiteboard_admin').addEventListener('keyup', (e) => this.updateWhiteboard());

        document.querySelectorAll('.team_select').forEach((select) => 
        {
            select.addEventListener('change', (e) => {
                let team = e.target.getAttribute('data-team');
                let num = e.target.getAttribute('data-num');
                this.updateTeam(e.target.value, team, num);
            });
        });
        document.querySelectorAll('.points_up, .points_down').forEach((select) => 
        {
            select.addEventListener('click', (e) => {
                let team = e.target.getAttribute('data-team');
                let inc = e.target.getAttribute('data-inc');
                this.incScore(team, inc);
            });
        });
    }
    updateTeam(conn_id, team, num)
    {
        this.send({ command:'updateTeam', conn_id: parseInt(conn_id,10), team: parseInt(team,10), num: parseInt(num,10) });
    }
    incScore(team, inc)
    {
        console.log('update team');
        this.send({ command:'incScore', team: parseInt(team,10), inc: parseInt(inc,10) });
    }

    updateWhiteboard()
    {
        this.send({ command:'whiteboard', text: document.querySelector('#whiteboard').value });

        console.log('update whiteboard');
    }

    showCards()
    {
        this.send({ command:'showCards'});
    }
    pickDiscarded()
    {
        this.send({ command:'pickDiscarded'});
    }

    error(data)
    {
        document.querySelector('#error').classList.remove('hidden');
        document.querySelector('#error').innerHTML=data.error;
        window.setTimeout(function()
        {
            document.querySelector('#error').classList.add('hidden');
        }, 1500);
    }
    close()
    {
        alert('Connection closed. Refresh the page...');
    }


    result_status(data)
    {
        let select = document.querySelector('.games select');
        let html='';
        let selected = select.value;
        data.games.forEach((game) => {
            html+='<option '+(game.id==selected ? 'selected':'')+' value="'+game.id+'">'+game.name+' ('+game.num_players+' / '+game.max+')</option>';
        });
        if(data.games.length===0)
        {
            html+='<option>-- No games played --</option>';
        }


        select.innerHTML=html;
        console.log('RECEIVED ',data);
    }

    joined()
    {
        document.querySelector('.games').classList.add('hidden');
        document.querySelector('.game').classList.remove('hidden');
    }

     update_game_status(data)
    {
        console.log('result join',data);

        document.querySelector('.game h1').innerText=data.game.name;
        document.querySelector('.cards_left').innerHTML=data.cards_left;
        document.querySelector('.cards_status').innerHTML=data.game.allvisible ? 'Visible':'Hidden';
        document.querySelector('.cards_status').classList.toggle('visible', data.game.allvisible);


        let spectators = [];
        Object.keys(data.game.spectators).forEach((s) => 
        {
            spectators.push(data.game.spectators[s].pseudo);
        });
        document.querySelector('.spectators').innerText =spectators.join(', ');

        let html ='';
        let player_keys = Object.keys(data.game.players);
        let is_master = false;

        let pseudo_options ='';
        for(let player_id in data.game.players)
        {
            pseudo_options += '<option value="'+player_id+'">'+data.game.players[player_id].pseudo+'</option>';
        }
        document.querySelector('#team1_1').innerHTML=pseudo_options;
        document.querySelector('#team1_2').innerHTML=pseudo_options;
        document.querySelector('#team2_1').innerHTML=pseudo_options;
        document.querySelector('#team2_2').innerHTML=pseudo_options;

        document.querySelector('#team1_1').value = data.game.teams[0][0];
        document.querySelector('#team1_2').value = data.game.teams[0][1];
        document.querySelector('#team2_1').value = data.game.teams[1][0];
        document.querySelector('#team2_2').value = data.game.teams[1][1];

        document.querySelector('.team_points.team_0').innerText = data.game.scores[0];
        document.querySelector('.team_points.team_1').innerText = data.game.scores[1];


        for(let i=0; i<data.game.teams.length;i++)
        {
            for(let j=0; j<data.game.teams[i].length; j++)
            {
                let player_id = data.game.teams[i][j];
                let player = data.game.players [player_id];
                console.log('ADD ',i,j);
                let pseudo='Waiting...';
                let actions='';
                let cards='';
                if(player)
                {
                    actions = '<button class="give" data-id="'+player.id+'">Give 1 card</button>';
                    pseudo = player.pseudo;
                    if(player.master)
                    {
                        pseudo+= ' <span class="master">(*DISTRIBUE*)</span>';
                        if(data.user_id==player.id)
                        {
                            is_master=true;
                        }
                    }
                    else
                    {
                        actions+= 
                            '<button class="kick" data-id="'+player.id+'">Kick player</button>'+
                            '<button class="set_master" data-id="'+player.id+'">Set Distribution</button>';
                    }
                    if(data.cards)
                    {
                        if(data.cards.players[player.id])
                        {
                            for(let i=0; i<data.cards.players[player.id].length;i++)
                            {
                                cards+='<div class="card">'+
                                    '<img src="img/'+data.cards.players[player.id][i]+'.gif" alt="" />'+
                                    '</div>';
                            }
                        }
                    }
                    else if(data.user_id==player.id)
                    {
                        for(let num=0; num<player.numcards;num++)
                        {
                            cards+='<div class="card">'+
                                '<img src="img/'+data.user_cards[num]+'.gif" alt="" />'+
                                '<button class="card-remove" data-num="'+data.user_cards[num]+'">Remove</button>'+
                                '</div>';
                        }
                    }
                    else
                    {
                        for(let i=0; i<player.numcards;i++)
                        {
                            cards+='<div class="card"><img src="img/50.gif" alt="" /></div>';
                        }
                    }
                }

                html+='<div class="game_pseudo">'+
                    '<div class="game_pseudo-actions hidden">'+actions+'</div>'+
                    '<div class="game_pseudo-content">'+
                    '<div class="game_pseudo-name">'+(pseudo)+'</div>'+
                    '<div class="cards">'+cards+'</div>'+
                    '</div>'+
                    '</div>';
            }
        }
        document.querySelector('.game_pseudos').innerHTML=html;

        console.log('is master',is_master);
        document.querySelector('.whiteboard_user pre').innerText = data.game.whiteboard;
        if(!is_master)
        {
            document.querySelector('.whiteboard_admin').value = data.game.whiteboard;
        }
        document.querySelectorAll('.game_pseudo-actions').forEach((d) => d.classList.toggle('hidden',!is_master));
        document.querySelectorAll('.game_action button').forEach((d) => d.classList.toggle('hidden',!is_master));
        document.querySelectorAll('.game_action select').forEach((d) => d.toggleAttribute('disabled',!is_master));
        document.querySelector('.whiteboard_admin').classList.toggle('hidden', !is_master);
        document.querySelector('.whiteboard_user').classList.toggle('hidden', is_master);

        document.querySelectorAll('.kick').forEach( (kick_btn) => 
        {
            console.log('bind ',kick_btn);
            kick_btn.addEventListener('click', (e) => {
                if(!confirm('Are you sure you want to KICK the player?'))
                {
                    return false;
                }
                let id = e.target.getAttribute('data-id');
                console.log('will kick ',id);
                this.send({ command:'kick', id: id});
            });
        });

        document.querySelectorAll('.set_master').forEach( (master_btn) => 
        {
            master_btn.addEventListener('click', (e) => {
                if(!confirm('Are you sure you want to give Master power to this user?'))
                {
                    return false;
                }
                let id = e.target.getAttribute('data-id');
                this.send({ command:'setMaster', id: id});
            });
        });

        document.querySelectorAll('.give').forEach( (master_btn) => 
        {
            master_btn.addEventListener('click', (e) => {
                let id = e.target.getAttribute('data-id');
                this.send({ command:'give', id: id});
            });
        });
        document.querySelectorAll('.card-remove').forEach( (master_btn) => 
        {
            master_btn.addEventListener('click', (e) => {
                let num = e.target.getAttribute('data-num');
                this.send({ command:'remove', num: parseInt(num,10)});
            });
        });

    }

    resize()
    {
    }
}

