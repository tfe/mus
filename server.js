#!/usr/bin/env node


var protocol = 'tfe-game_v0';
var fs = require('fs'), path = require('path');

var ssl_enabled = true;
process.argv.forEach(function(val, index, array)
{
    if(val=='-nossl')
    {
        ssl_enabled=false;
    }
});

const musCards = [
     1, 2, 3, 4, 5, 6, 7,10,11,12, // Oros
    13,14,15,16,17,18,19,22,23,24, // Copa
    25,26,27,28,29,30,31,34,35,36, // Espadas
    37,38,39,40,41,42,43,46,47,48
];

const http = ssl_enabled ? require('https') : require('http');

const server = ssl_enabled ?
    new http.createServer({
        cert: fs.readFileSync('/var/www/letsencrypt/live/tfe.eus/fullchain.pem'),
        key: fs.readFileSync('/var/www/letsencrypt/live/tfe.eus/privkey.pem')
    }) : 
    new http.createServer({ });
    ;
server.listen(6060, function() {});

var WebSocketServer = require('websocket').server;
 
wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});
 
function originIsAllowed(origin) {
  return true;
}
 
var last_id = 1;
var connections= [];
var games= [];
var gameCards= {};


const methods=
{
    send: function(conn, data)
    {
        conn.sendUTF(JSON.stringify(data));
    },
    broadcast: function(msg)
    {
        //console.log('send to all ',msg);
        Object.keys(connections).forEach((s) => 
        {
            this.send(connections[s], msg);
        });
    },

    create: function(conn, data)
    {
        if(!data.max || data.max>4)
        {
            return this.send(conn, {
                command: 'error',
                error:'Max number of player is: 4!'
            });
        }
        let id = Math.floor(Math.random()*10000);
        let found = false;
        games.forEach((game) => {
            if(game.name == data.name)
            {
                found = true;
            }
        });
        if(found)
        {
            return this.send(conn, {
                command: 'error',
                error:'A game with this name is already created.'
            });
        }
        data.id = id;
        data.allvisible=false;
        data.players = {};
        data.teams = [[null,null], [null,null]];
        data.scores = [0,0];
        data.spectators = {};
        data.whiteboard='';
        data.num_players = 0;
        games.push(data);

        this.newCards(data);

        // Force user to join
        this.join(conn,{ command:'join', id: id});
        this.updateAllStatus();
    },
    incScore(conn, data)
    {
        games.forEach((game) => {
            if(game.players[conn.id] && game.players[conn.id].master)
            {
                game.scores[data.team] += data.inc;
                this.updateGameStatus(game);
            }
        });
    },

    kick(conn, data)
    {
        games.forEach((game) => {
            if(game.players[conn.id] && game.players[conn.id].master)
            {
                this.send(connections[data.id], { command: 'leaveGame' });
            }
        });
    },

    remove(conn, data)
    {
        games.forEach((game) => {
            if(game.players[conn.id])
            {
                if(gameCards[game.name].players[conn.id])
                {
                    let userCards = gameCards[game.name].players[conn.id]
                    let idx = userCards.indexOf(data.num);
                    if(idx>-1)
                    {
                        console.log('Remove card ',data.num);
                        userCards.splice(idx, 1);
                        gameCards[game.name].discarded.push(data.num);
                        game.players[conn.id].numcards--;
                        this.updateGameStatus(game);
                    }
                    else
                    {
                        console.log('cannot find card ',data.num+' in ',userCards,' = ',idx);
                    }
                }
            }
        });
    },

    setMaster(conn, data)
    {
        console.log('set master',data);
        games.forEach((game) => {
            let was_master = false;
            if(game.players[conn.id] && game.players[conn.id].master)
            {
                was_master=true;
                game.players[conn.id].master = false;
                game.players[data.id].master = true;
                this.nextGame(conn);
                this.updateGameStatus(game);
            }
        });
    },
    whiteboard(conn, data)
    {
        console.log('set master',data);
        games.forEach((game) => {
            if(game.players[conn.id] && game.players[conn.id].master)
            {
                game.whiteboard = data.text;
                this.updateGameStatus(game);
            }
        });
    },

    updateTeam(conn,data)
    {
        console.log('have to updat team for ',data.conn_id, data.team, data.num);
        games.forEach((game) => {
            if(game.players[conn.id] && game.players[conn.id].master)
            {
                let dest_team = data.team;
                let dest_team_idx = data.num;

                let source_team = game.players[data.conn_id].team;
                let source_team_idx = game.teams[source_team].indexOf(data.conn_id);

                console.log('FROM ',source_team+'/'+source_team_idx+' TO ',dest_team+'/'+dest_team_idx);
                // if dest is occupied, switch
                if(game.teams[dest_team][dest_team_idx]!==null)
                {
                    let prev = game.teams[dest_team][dest_team_idx]
                    game.teams[dest_team][dest_team_idx] = data.conn_id;
                    game.teams[source_team][source_team_idx] = prev;

                    game.players[prev].team = source_team;
                    game.players[data.conn_id].team = dest_team;
                }
                else
                {
                    game.teams[source_team][source_team_idx]=null;
                    game.teams[dest_team][dest_team_idx] = data.conn_id;
                    game.players[data.conn_id].team = dest_team;
                }

                this.updateGameStatus(game);
            }
        });
    },

    leave(conn)
    {
        games.forEach((game, game_idx) => {
            Object.keys(game.spectators).forEach((conn_id) => {
                if(conn_id == conn.id)
                {
                    delete(game.spectators[conn_id]);
                }
            });
            Object.keys(game.players).forEach((conn_id) => {
                if(conn_id == conn.id)
                {
                    game.num_players--;
                    let was_master = game.players[conn.id].master;
                    let team = game.teams[game.players[conn.id].team];
                    team[team.indexOf(conn.id)]=null;
                    delete(game.players[conn.id]);
                    if(was_master && Object.keys(game.players).length>0)
                    {
                        console.log('set new master to ',Object.keys(game.players)[0], game);
                        game.players[Object.keys(game.players)[0]].master = true;
                    }
                }
            });
            if(Object.keys(game.players).length===0)
            {
                games.splice(game_idx, 1);
            }
            else
            {
                this.updateGameStatus(game);
            }
        });
        this.updateAllStatus();
    },

    newCards(game)
    {
        console.log('new cards for ',game);
        let cards = [...musCards];
        game.num_cards = cards.length;

        gameCards[game.name] = { notused: cards, discarded: [], players:{ } };
    },

    nextGame(conn)
    {
        games.forEach((game) => {
            Object.keys(game.players).forEach((conn_id) => {
                // This is the correct game we are searching for
                if(conn_id == conn.id)
                {
                    this.newCards(game);
                    game.allvisible = false;
                    Object.keys(game.players).forEach((conn_id) => {
                        game.players[conn_id].numcards=0;
                    });
                }
                this.updateGameStatus(game);
            });
        });
    },

    pickDiscarded(conn)
    {
        games.forEach((game) => {
            Object.keys(game.players).forEach((conn_id) => {
                // This is the correct game we are searching for
                if(conn_id == conn.id)
                {
                    gameCards[game.name].notused=  gameCards[game.name].notused.concat(gameCards[game.name].discarded);
                    gameCards[game.name].discarded=[];
                }
                this.updateGameStatus(game);
            });
        });
    },

    showCards(conn)
    {
        games.forEach((game) => {
            Object.keys(game.players).forEach((conn_id) => {
                if(conn_id == conn.id)
                {
                    game.allvisible = true;
                }
                this.updateGameStatus(game);
            });
        });
    },

    giveCard(game, conn_id)
    {
        let usedCards = gameCards[game.name].notused;
        if(!usedCards.length)
        {
            return this.broadcast({
                'command': 'error',
                'error': 'No more cards.',
            });
        }
        let idx = Math.floor(Math.random()*usedCards.length);
        let card = usedCards[idx];
        usedCards.splice(idx, 1);

        if(!gameCards[game.name].players[conn_id])
        {
            gameCards[game.name].players[conn_id] = [];
        }
        game.players[conn_id].numcards++;
        gameCards[game.name].players[conn_id].push(card);
        console.log('Player card: ', gameCards[game.name].players[conn_id]);
        this.updateGameStatus(game);
    },

    give(conn, data)
    {
        games.forEach((game) => {
            Object.keys(game.players).forEach((conn_id) => {
                if(conn_id == data.id)
                {
                    let card = this.giveCard(game, conn_id);
                }
            });
        });
    },

    updateGameStatus(game)
    {
        Object.keys(game.spectators).forEach((conn_id) => {
            this.send(connections[conn_id], {
                command: 'update_game_status',
                user_id: conn_id,
                cards: gameCards[game.name],
                cards_left: gameCards[game.name].notused.length,
                game: game
            });
        });
        Object.keys(game.players).forEach((conn_id) => {
            let user_cards = gameCards[game.name].players[conn_id];

            if(game.allvisible)
            {
                this.send(connections[conn_id], {
                    command: 'update_game_status',
                    user_id: conn_id,
                    cards: gameCards[game.name],
                    cards_left: gameCards[game.name].notused.length,
                    user_cards: user_cards,
                    game: game
                });
            }
            else
            {
                this.send(connections[conn_id], {
                    command: 'update_game_status',
                    user_id: conn_id,
                    user_cards: user_cards,
                    cards_left: gameCards[game.name].notused.length,
                    game: game
                });
            }
        });
    },

    join(conn, data)
    {
        games.forEach((game) => {
            if(game.id == data.id)
            {
                if(game.max > game.num_players)
                {
                    game.num_players++;
                    let team = (game.teams[0][0]===null || game.teams[0][1]===null) ? 0 : 1;
                    console.log('AFTER= ',team);
                    game.players[conn.id]={team: team, pseudo: conn.pseudo, id:conn.id, master: Object.keys(game.players).length===0, numcards: 0 };

                    let team_data = game.teams[team];
                    if(team_data[0]===null) { team_data[0]=conn.id; }
                    else if(team_data[1]===null) { team_data[1]=conn.id; }

                    this.send(conn, {
                        command: 'joined'
                    });
                    this.updateGameStatus(game);
                }
                else
                {
                    this.send(conn, {
                        command: 'error',
                        error:'Max players.'
                    });
                }
            }
        });
        this.updateAllStatus();
    },

    spectator(conn, data)
    {
        games.forEach((game) => {
            if(game.id == data.id)
            {
                game.spectators[conn.id]={ pseudo: conn.pseudo, id:conn.id  };
                this.send(conn, {
                    command: 'joined'
                });
                this.updateGameStatus(game);
            }
        });
        this.updateAllStatus();
    },

    updateAllStatus()
    {
        let pseudos = [];
        Object.keys(connections).forEach((s) => 
        {
            pseudos.push(connections[s].pseudo);
        });
        let sendgames = [];
        (games).forEach((game) => 
        {
            sendgames.push({
                id: game.id,
                max: game.max,
                num_players: game.num_players,
                name: game.name
            });
        });
        this.broadcast({
            'result': true,
            'command': 'result_status',
            'users': pseudos,
            'games': sendgames,
        });
    },

    setName: function(conn, data)
    {
        conn.pseudo = data.pseudo;
        this.send(conn, {
            command: 'result_setName'
        });
        this.updateAllStatus();
    },

    disconnect: function(conn)
    {
        console.log('DISCONNECTED!', conn.id);
        games.forEach((game) => {
            Object.keys(game.players).forEach((conn_id) => {
                if(conn.id == conn_id)
                {
                    this.leave(conn);
                }
            });
            Object.keys(game.spectators).forEach((conn_id) => {
                if(conn.id == conn_id)
                {
                    this.leave(conn);
                }
            });
        });
    }
};

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    // Only accept our great protocol
    if (request.requestedProtocols.indexOf(protocol) === -1) {
        return request.reject();
    }
    
    var connection = request.accept(protocol, request.origin);
    var connection_id = ++last_id;
    connection.id= connection_id;


    connections[last_id] = connection;



    connection.on('message', function(message) {
        //console.log('received ',message);
        if (message.type === 'utf8') {
            try
            {
                var msg=null;
                try
                {
                    msg = JSON.parse(message.utf8Data);
                }
                catch(err)
                {
                    console.error('invalid message received.');
                    return false;
                }
                if(!msg || !msg.command)
                {
                    console.error('invalid message received.');
                    return false;
                }
                if(msg.command == 'connected')
                {
                }
                if(methods[msg.command])
                {
                    methods[msg.command](connection, msg);
                }
                else
                {
                }
            }
            catch(err)
            {
                console.log('cannot parse '+message.utf8Data, msg.command, err);
            }
            //connection.sendUTF(JSON.stringify({ action:'ok', message: message.utf8Data}));
        }
    });
    connection.on('close', function(reasonCode, description) {
        if(methods.disconnect)
        {
            methods.disconnect(connections[connection_id]);
        }
        methods.leave(connections[connection_id]);
        connections[connection_id]={};
        delete connections[connection_id];
    });
});
